from bottle import route, run
from os import environ


@route('/')
def hello():
    return "Hello from Smart OS"


if __name__ == '__main__':
    run(host='0.0.0.0', port=environ.get('PORT', 8080))
